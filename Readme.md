# Dev.XYZ Full Stack Dev Test
## How To Take This Test
 * Fork this repo
 * Push your results up to origin upon completing the test
 * Email a link to your repo to the user who sent the test to you 
  
## Instructions
Your mission, should you choose to accept it, is to create a persistent, responsive meme management system.

As a user I should be able to:

 * add a new meme into the system
    * upload an image
    * associate a title with it
    * associate one or more tags with it
 * list all existing memes in the system
 * view an individual existing meme 
 * edit a meme's title and tag
 * search for a meme by title or tag
 * delete a meme

## Rules
 * This is a timed test
 * Results must be pushed up to origin within 4 hours of being sent to you 
 * Your backend language should be PHP
 * You can use any PHP framework / library you wish, the following are recommended
    * Laravel
    * Slim
    * Lumen
    * Zend
 * You can use any JS framework / library you wish, the following are recommended
    * Angular 1/2
    * Backbone
    * React
 * You can use any CSS library you wish, the following are recommended
    * Hand-coded CSS
    * Zurb foundation
    * Twitter bootstrap
 * You can use any DB you wish, including a flat file system
 * Precompilers like SASS and Coffeescript are allowed
 * Scaffolding the CRUD interface is *not* allowed